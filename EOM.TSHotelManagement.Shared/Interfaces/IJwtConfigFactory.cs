﻿namespace EOM.TSHotelManagement.Shared
{
    public interface IJwtConfigFactory
    {
        JwtConfig GetJwtConfig();
    }
}
