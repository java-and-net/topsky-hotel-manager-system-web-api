﻿using System;

namespace EOM.TSHotelManagement.Common.Util
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NeedValidAttribute : Attribute
    {
    }
}
