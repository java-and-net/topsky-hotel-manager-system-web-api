﻿using System;

namespace EOM.TSHotelManagement.Common.Util
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class UIDisplayAttribute : Attribute
    {
        public string DisplayName { get; }

        public bool IsNumber { get; set; } = false;

        public UIDisplayAttribute(string displayName,bool isNumber = false)
        {
            DisplayName = displayName;
            IsNumber = isNumber;
        }
    }
}
