﻿using EOM.TSHotelManagement.Application;
using EOM.TSHotelManagement.Common.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace EOM.TSHotelManagement.WebApi.Controllers
{
    /// <summary>
    /// 水电信息控制器
    /// </summary>
    public class HydroelectricityController : ControllerBase
    {
        /// <summary>
        /// 水电信息服务
        /// </summary>
        private readonly IHydroelectricityService hydroelectricPowerService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="hydroelectricPowerService"></param>
        public HydroelectricityController(IHydroelectricityService hydroelectricPowerService)
        {
            this.hydroelectricPowerService = hydroelectricPowerService;
        }

        /// <summary>
        /// 根据条件查询水电费信息
        /// 替换了 SelectWtiInfoByRoomNo, SelectWtiInfoByRoomNoAndTime, ListWtiInfoByRoomNo, SelectWtiInfoAll
        /// </summary>
        /// <param name="roomNo">房间号（可选）</param>
        /// <param name="useDate">使用开始时间（可选）</param>
        /// <param name="endDate">使用结束时间（可选）</param>
        /// <returns>符合条件的水电费信息列表</returns>
        [HttpGet]
        public List<Hydroelectricity> SelectWtiInfo([FromQuery] string roomNo, [FromQuery] DateTime? useDate, [FromQuery] DateTime? endDate)
        {
            return this.hydroelectricPowerService.SelectWtiInfo(roomNo, useDate, endDate);
        }

        /// <summary>
        /// 添加水电费信息
        /// 替换了 InsertWtiInfo
        /// </summary>
        /// <param name="w"></param>
        /// <returns></returns>
        [HttpPost]
        public bool InsertWtiInfo([FromBody] Hydroelectricity w)
        {
            return this.hydroelectricPowerService.InsertWtiInfo(w);
        }

        /// <summary>
        /// 修改水电费信息
        /// 替换了 UpdateWtiInfo 和 UpdateWtiInfoByRoomNoAndDateTime
        /// </summary>
        /// <param name="w">包含要修改的数据，以及WtiNo作为查询条件</param>
        /// <returns></returns>
        [HttpPost]
        public bool UpdateWtiInfo([FromBody] Hydroelectricity w)
        {
            return this.hydroelectricPowerService.UpdateWtiInfo(w);
        }


        /// <summary>
        /// 根据房间编号、使用时间删除水电费信息
        /// 替换了 DeleteWtiInfoByRoomNoAndDateTime
        /// </summary>
        /// <param name="roomno"></param>
        /// <param name="usedate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        [HttpPost]
        public bool DeleteWtiInfo([FromBody] Hydroelectricity hydroelectricity)
        {
            return this.hydroelectricPowerService.DeleteWtiInfo(hydroelectricity);
        }
    }
}
