﻿using EOM.TSHotelManagement.Application;
using EOM.TSHotelManagement.Common.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EOM.TSHotelManagement.WebApi.Controllers
{
    /// <summary>
    /// 菜单控制器
    /// </summary>
    public class MenuController : ControllerBase
    {
        private readonly IMenuService menuService;

        public MenuController(IMenuService menuService)
        {
            this.menuService = menuService;
        }

        /// <summary>
        /// 查询所有菜单信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Menu> SelectMenuAll()
        {
            return menuService.SelectMenuAll();
        }

        /// <summary>
        /// 构建菜单树
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public List<MenuViewModel> BuildMenuAll()
        {
            return menuService.BuildMenuAll();
        }

        /// <summary>
        /// 插入菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        [HttpPost]
        public bool InsertMenu([FromBody]Menu menu)
        {
            return menuService.InsertMenu(menu);
        }

        /// <summary>
        /// 更新菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        [HttpPost]
        public bool UpdateMenu([FromBody] Menu menu)
        {
            return menuService.UpdateMenu(menu);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        [HttpPost]
        public bool DeleteMenu([FromBody] Menu menu)
        {
            return menuService.DeleteMenu(menu);
        }
    }
}
