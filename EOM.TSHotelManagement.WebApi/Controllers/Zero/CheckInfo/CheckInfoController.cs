﻿using EOM.TSHotelManagement.Application;
using EOM.TSHotelManagement.Common.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EOM.TSHotelManagement.WebApi.Controllers
{
    /// <summary>
    /// 监管统计控制器
    /// </summary>
    public class CheckInfoController : ControllerBase
    {
        /// <summary>
        /// 监管统计
        /// </summary>
        private readonly ICheckInfoService checkInfoService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="checkInfoService"></param>
        public CheckInfoController(ICheckInfoService checkInfoService)
        {
            this.checkInfoService = checkInfoService;
        }

        /// <summary>
        /// 查询所有监管统计信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<CheckInfo> SelectCheckInfoAll()
        {
            return checkInfoService.SelectCheckInfoAll();
        }

        /// <summary>
        /// 插入监管统计信息
        /// </summary>
        /// <param name="checkInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public bool InsertCheckInfo([FromBody]CheckInfo checkInfo)
        {
            return checkInfoService.InsertCheckInfo(checkInfo);
        }

        /// <summary>
        /// 更新监管统计信息
        /// </summary>
        /// <param name="checkInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public bool UpdateCheckInfo([FromBody]CheckInfo checkInfo)
        {
            return checkInfoService.UpdateCheckInfo(checkInfo);
        }

        /// <summary>
        /// 删除监管统计信息
        /// </summary>
        /// <param name="checkInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public bool DeleteCheckInfo([FromBody] CheckInfo checkInfo)
        {
            return checkInfoService.DeleteCheckInfo(checkInfo);
        }
    }
}
