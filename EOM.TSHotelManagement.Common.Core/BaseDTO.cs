﻿using System;

namespace EOM.TSHotelManagement.Common.Core
{
    public class BaseDTO
    {

        /// <summary>
        /// 删除标识
        /// </summary>
        [SqlSugar.SugarColumn(ColumnName = "delete_mk")]
        public int? IsDelete { get; set; }
        /// <summary>
        /// 资料创建人
        /// </summary>
        [SqlSugar.SugarColumn(ColumnName = "datains_usr", IsOnlyIgnoreUpdate = true)]
        public string DataInsUsr { get; set; }
        /// <summary>
        /// 资料创建时间
        /// </summary>
        [SqlSugar.SugarColumn(ColumnName = "datains_date", IsOnlyIgnoreUpdate = true)]
        public DateTime? DataInsDate { get; set; }
        /// <summary>
        /// 资料更新人
        /// </summary>
        [SqlSugar.SugarColumn(ColumnName = "datachg_usr", IsOnlyIgnoreInsert = true)]
        public string DataChgUsr { get; set; }
        /// <summary>
        /// 资料更新时间
        /// </summary>
        [SqlSugar.SugarColumn(ColumnName = "datachg_date", IsOnlyIgnoreInsert = true)]
        public DateTime? DataChgDate { get; set; }
        /// <summary>
        /// Token
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public string UserToken { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public int Page { get; set; } = 0;
        /// <summary>
        /// 总数
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public int PageSize { get; set; } = 10;
    }
}
