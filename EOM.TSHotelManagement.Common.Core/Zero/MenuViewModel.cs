﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOM.TSHotelManagement.Common.Core
{
    /// <summary>
    /// 菜单视图模型
    /// </summary>
    public class MenuViewModel
    {
        /// <summary>
        /// 菜单主键
        /// </summary>
        public string key { get; set; }
        /// <summary>
        /// 菜单标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 菜单路径
        /// </summary>
        public string path { get; set; }
        /// <summary>
        /// 子菜单
        /// </summary>
        public List<MenuViewModel> children { get; set; }
    }
}
