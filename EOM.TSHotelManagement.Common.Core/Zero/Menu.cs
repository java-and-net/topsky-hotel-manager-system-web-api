﻿/*
 * MIT License
 *Copyright (c) 2021 易开元(Easy-Open-Meta)

 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:

 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.

 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 *
 *模块说明：菜单表
 */
using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace EOM.TSHotelManagement.Common.Core
{
    /// <summary>
    /// 菜单表
    ///</summary>
    [SugarTable("menu")]
    public class Menu: BaseDTO
    {
        /// <summary>
        /// 备  注:自增长ID
        /// 默认值:
        ///</summary>
        [SugarColumn(ColumnName="id" ,IsPrimaryKey = true) ]
        public int Id  { get; set;  } 
     
        /// <summary>
        /// 备  注:菜单键
        /// 默认值:
        ///</summary>
        [SugarColumn(ColumnName="key" ) ]
        public string? Key  { get; set;  } 
     
        /// <summary>
        /// 备  注:菜单标题
        /// 默认值:
        ///</summary>
        [SugarColumn(ColumnName="title" ) ]
        public string Title  { get; set;  } = null!;
     
        /// <summary>
        /// 备  注:菜单路径
        /// 默认值:
        ///</summary>
        [SugarColumn(ColumnName="path" ) ]
        public string? Path  { get; set;  } 
     
        /// <summary>
        /// 备  注:父级ID
        /// 默认值:
        ///</summary>
        [SugarColumn(ColumnName="parent" ) ]
        public int? Parent  { get; set;  } 
    

    }
    
}