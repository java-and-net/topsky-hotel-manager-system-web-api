﻿/*
 * MIT License
 *Copyright (c) 2021 易开元(Easy-Open-Meta)

 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:

 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.

 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 *
 */
using EOM.TSHotelManagement.Common.Core;
using EOM.TSHotelManagement.EntityFramework;
using SqlSugar;
using System.Linq.Expressions;

namespace EOM.TSHotelManagement.Application
{
    /// <summary>
    /// 水电信息接口实现类
    /// </summary>
    public class HydroelectricityService : IHydroelectricityService
    {
        /// <summary>
        /// 水电信息
        /// </summary>
        private readonly GenericRepository<Hydroelectricity> wtiRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wtiRepository"></param>
        public HydroelectricityService(GenericRepository<Hydroelectricity> wtiRepository)
        {
            this.wtiRepository = wtiRepository;
        }

        #region 查询水电费信息

        /// <summary>
        /// 根据条件查询水电费信息
        /// </summary>
        /// <param name="roomNo">房间号（可选）</param>
        /// <param name="useDate">使用开始时间（可选）</param>
        /// <param name="endDate">使用结束时间（可选）</param>
        /// <returns>符合条件的水电费信息列表</returns>
        public List<Hydroelectricity> SelectWtiInfo(string roomNo = null, DateTime? useDate = null, DateTime? endDate = null)
        {
            var repository = wtiRepository;

            var where = Expressionable.Create<Hydroelectricity>();

            where = where.And(a => a.IsDelete != 1);

            if (!string.IsNullOrEmpty(roomNo))
            {
                where = where.And(a => a.RoomNo.Equals(roomNo));
            }

            if (useDate.HasValue)
            {
                where = where.And(a => a.UseDate >= useDate.Value);
            }

            if (endDate.HasValue)
            {
                where = where.And(a => a.EndDate >= endDate.Value);
            }

            var listSource = repository.GetList(where.ToExpression());

            return listSource;
        }
        #endregion

        #region 添加水电费信息
        /// <summary>
        /// 添加水电费信息
        /// </summary>
        /// <param name="w"></param>
        /// <returns></returns>
        public bool InsertWtiInfo(Hydroelectricity w)
        {
            return wtiRepository.Insert(w);
        }
        #endregion

        #region 修改水电费信息
        /// <summary>
        /// 修改水电费信息
        /// </summary>
        /// <param name="w">包含要修改的数据，以及WtiNo作为查询条件</param>
        /// <returns></returns>
        public bool UpdateWtiInfo(Hydroelectricity w)
        {
            return wtiRepository.Update(a => new Hydroelectricity()
            {
                UseDate = w.UseDate,
                EndDate = w.EndDate,
                WaterUse = w.WaterUse,
                PowerUse = w.PowerUse,
                Record = w.Record,
                CustoNo = w.CustoNo,
                DataChgUsr = w.DataChgUsr,
                RoomNo = w.RoomNo
            }, a => a.WtiNo == w.WtiNo);
        }
        #endregion

        #region 根据房间编号、使用时间删除水电费信息
        /// <summary>
        /// 根据房间编号、使用时间删除水电费信息
        /// </summary>
        /// <returns></returns>
        public bool DeleteWtiInfo(Hydroelectricity hydroelectricity)
        {
            return wtiRepository.Update(a => new Hydroelectricity()
            {
                IsDelete = 1
            }, a => a.WtiNo == hydroelectricity.WtiNo);
        }
        #endregion
    }
}
