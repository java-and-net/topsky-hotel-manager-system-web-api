﻿/*
 * MIT License
 *Copyright (c) 2021 易开元(Easy-Open-Meta)

 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:

 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.

 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 *
 */
using EOM.TSHotelManagement.Common.Core;

/// <summary>
/// 水电信息接口
/// </summary>
public interface IHydroelectricityService
{
    /// <summary>
    /// 根据条件查询水电费信息
    /// 替换了 SelectWtiInfoByRoomNo, SelectWtiInfoByRoomNoAndTime, ListWtiInfoByRoomNo, SelectWtiInfoAll
    /// </summary>
    /// <param name="roomNo">房间号（可选）</param>
    /// <param name="useDate">使用开始时间（可选）</param>
    /// <param name="endDate">使用结束时间（可选）</param>
    /// <returns>符合条件的水电费信息列表</returns>
    List<Hydroelectricity> SelectWtiInfo(string roomNo = null, DateTime? useDate = null, DateTime? endDate = null);

    /// <summary>
    /// 添加水电费信息
    /// 替换了 InsertWtiInfo
    /// </summary>
    /// <param name="w"></param>
    /// <returns></returns>
    bool InsertWtiInfo(Hydroelectricity w);

    /// <summary>
    /// 修改水电费信息
    /// 替换了 UpdateWtiInfo 和 UpdateWtiInfoByRoomNoAndDateTime
    /// </summary>
    /// <param name="w">包含要修改的数据，以及WtiNo作为查询条件</param>
    /// <returns></returns>
    bool UpdateWtiInfo(Hydroelectricity w);

    /// <summary>
    /// 根据房间编号、使用时间删除水电费信息
    /// 替换了 DeleteWtiInfoByRoomNoAndDateTime
    /// </summary>
    /// 
    /// <returns></returns>
    bool DeleteWtiInfo(Hydroelectricity hydroelectricity);
}
