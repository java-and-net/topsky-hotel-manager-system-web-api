﻿/*
 * MIT License
 *Copyright (c) 2021 易开元(Easy-Open-Meta)

 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:

 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.

 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 *
 */
using EOM.TSHotelManagement.Common.Core;
using EOM.TSHotelManagement.EntityFramework;
using jvncorelib.EntityLib;

namespace EOM.TSHotelManagement.Application
{
    /// <summary>
    /// 菜单接口实现类
    /// </summary>
    public class MenuService : IMenuService
    {
        /// <summary>
        /// 菜单
        /// </summary>
        private readonly GenericRepository<Menu> menuRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuRepository"></param>
        public MenuService(GenericRepository<Menu> menuRepository)
        {
            this.menuRepository = menuRepository;
        }

        /// <summary>
        /// 构建菜单树
        /// </summary>
        /// <returns></returns>
        public List<MenuViewModel> BuildMenuAll()
        {
            List<Menu> allMenus = menuRepository.GetList(a => a.IsDelete != 1).OrderBy(a => a.Id).ToList();

            List<MenuViewModel> result = BuildMenuTree(allMenus, null);

            return result;
        }

        /// <summary>
        /// 查询所有菜单信息
        /// </summary>
        /// <returns></returns>
        public List<Menu> SelectMenuAll()
        {
            List<Menu> allMenus = menuRepository.GetList().ToList();

            return allMenus;
        }

        /// <summary>
        /// 插入菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public bool InsertMenu(Menu menu)
        {
            return menuRepository.Insert(menu);
        }

        /// <summary>
        /// 更新菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public bool UpdateMenu(Menu menu)
        {
            return menuRepository.Update(menu);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public bool DeleteMenu(Menu menu)
        {
            return menuRepository.Delete(menu);
        }

        /// <summary>
        /// 递归构建菜单树
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private List<MenuViewModel> BuildMenuTree(List<Menu> menus, int? parentId)
        {
            List<MenuViewModel> result = new List<MenuViewModel>();

            var filteredMenus = menus.Where(m => m.Parent == parentId).ToList();

            foreach (var menu in filteredMenus)
            {
                MenuViewModel viewModel = new MenuViewModel
                {
                    key = menu.Key,
                    title = menu.Title,
                    path = menu.Path,
                    children = BuildMenuTree(menus, menu.Id)
                };
                if (viewModel.children.Count == 0)
                {
                    viewModel.children = null;
                }
                result.Add(viewModel);
            }

            return result;
        }
    }
}
