﻿/*
 * MIT License
 *Copyright (c) 2021 易开元(Easy-Open-Meta)

 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:

 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.

 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 *
 */
using EOM.TSHotelManagement.Common.Core;
using EOM.TSHotelManagement.EntityFramework;

namespace EOM.TSHotelManagement.Application
{
    /// <summary>
    /// 会员等级规则功能模块接口实现类
    /// </summary>
    public class VipRuleAppService : IVipRuleAppService
    {
        /// <summary>
        /// 会员等级规则
        /// </summary>
        private readonly GenericRepository<VipRule> vipRuleRepository;

        /// <summary>
        /// 客户类型
        /// </summary>
        private readonly GenericRepository<CustoType> custoTypeRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vipRuleRepository"></param>
        /// <param name="custoTypeRepository"></param>
        public VipRuleAppService(GenericRepository<VipRule> vipRuleRepository, GenericRepository<CustoType> custoTypeRepository)
        {
            this.vipRuleRepository = vipRuleRepository;
            this.custoTypeRepository = custoTypeRepository;
        }

        /// <summary>
        /// 查询会员等级规则列表
        /// </summary>
        /// <returns></returns>
        public List<VipRule> SelectVipRuleList()
        {
            List<VipRule> vipRules = new List<VipRule>();

            var listSource = vipRuleRepository.GetList(a => a.IsDelete != 1);

            var listUserType = custoTypeRepository.GetList(a => a.IsDelete != 1);

            listSource.ForEach(source =>
            {
                var userType = listUserType.FirstOrDefault(a => a.UserType == source.TypeId);
                source.TypeName = userType == null ? "" : userType.TypeName;
            });

            vipRules = listSource;

            return vipRules;
        }

        /// <summary>
        /// 查询会员等级规则
        /// </summary>
        /// <param name="vipRule"></param>
        /// <returns></returns>
        public VipRule SelectVipRule(VipRule vipRule)
        {
            VipRule vipRule1 = new VipRule();

            var source = vipRuleRepository.GetSingle(a => a.RuleId.Equals(vipRule.RuleId));

            var userType = custoTypeRepository.GetSingle(a => a.UserType == source.TypeId);
            source.TypeName = userType == null ? "" : userType.TypeName;

            vipRule1 = source;

            return vipRule1;
        }

        /// <summary>
        /// 添加会员等级规则
        /// </summary>
        /// <param name="vipRule"></param>
        /// <returns></returns>
        public bool AddVipRule(VipRule vipRule)
        {
            return vipRuleRepository.Insert(new VipRule()
            {
                RuleId = vipRule.RuleId,
                RuleName = vipRule.RuleName,
                RuleValue = vipRule.RuleValue,
                TypeId = vipRule.TypeId,
                IsDelete = 0,
                DataInsUsr = vipRule.DataInsUsr,
            });
        }

        /// <summary>
        /// 删除会员等级规则
        /// </summary>
        /// <param name="vipRule"></param>
        /// <returns></returns>
        public bool DelVipRule(VipRule vipRule)
        {
            return vipRuleRepository.Update(a => new VipRule
            {
                IsDelete = 1,
                DataChgUsr = vipRule.DataChgUsr,
            }, a => a.RuleId == vipRule.RuleId);
        }

        /// <summary>
        /// 更新会员等级规则
        /// </summary>
        /// <param name="vipRule"></param>
        /// <returns></returns>
        public bool UpdVipRule(VipRule vipRule)
        {
            return vipRuleRepository.Update(a => new VipRule
            {
                RuleName = vipRule.RuleName,
                RuleValue = vipRule.RuleValue,
                IsDelete = vipRule.IsDelete,
                DataChgUsr = vipRule.DataChgUsr,
                DataChgDate = vipRule.DataChgDate
            }, a => a.RuleId == vipRule.RuleId);
        }
    }
}
