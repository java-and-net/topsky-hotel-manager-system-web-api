#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["topsky-hotel-management-system-web-api/EOM.TSHotelManagement.WebApi/EOM.TSHotelManagement.WebApi.csproj", "topsky-hotel-management-system-web-api/EOM.TSHotelManagement.WebApi/"]
COPY ["topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Shared/EOM.TSHotelManagement.Shared.csproj", "topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Shared/"]
COPY ["topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Application/EOM.TSHotelManagement.Application.csproj", "topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Application/"]
COPY ["topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Common.Core/EOM.TSHotelManagement.Common.Core.csproj", "topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Common.Core/"]
COPY ["topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Common.Util/EOM.TSHotelManagement.Common.Util.csproj", "topsky-hotel-management-system-web-api/EOM.TSHotelManagement.Common.Util/"]
COPY ["topsky-hotel-management-system-web-api/EOM.TSHotelManagement.EntityFramework/EOM.TSHotelManagement.EntityFramework.csproj", "topsky-hotel-management-system-web-api/EOM.TSHotelManagement.EntityFramework/"]
RUN dotnet restore "./topsky-hotel-management-system-web-api/EOM.TSHotelManagement.WebApi/EOM.TSHotelManagement.WebApi.csproj"
COPY . .
WORKDIR "/src/topsky-hotel-management-system-web-api/EOM.TSHotelManagement.WebApi"
RUN dotnet build "./EOM.TSHotelManagement.WebApi.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./EOM.TSHotelManagement.WebApi.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "EOM.TSHotelManagement.WebApi.dll"]